$(document).ready(function () {


    pageSize = 12;
    pagesCount = $(".content-thumb").length;
    console.log($(".content-thumb").length);
    var currentPage = 1;

    /////////// PREPARE NAV ///////////////
    var nav = '';
    var totalPages = Math.ceil(pagesCount / pageSize);
    console.log(totalPages);
    for (var s = 0; s < totalPages; s++) {
        nav += '<li class="numeros"><a href="#thumbs">' + (s + 1) + '</a></li>';
    }
    $(".pag_prev").after(nav);
    $(".numeros").first().addClass("active");
    //////////////////////////////////////

    showPage = function () {
        $(".content-thumb").hide().each(function (n) {
            if (n >= pageSize * (currentPage - 1) && n < pageSize * currentPage)
                $(this).show();
        });
    };
    showPage();


    $(".pagination li.numeros").click(function () {
        $(".pagination li").removeClass("active");
        $(this).addClass("active");
        currentPage = parseInt($(this).text());
        showPage();
    });

    $(".pagination li.pag_prev").click(function () {
        if ($(this).next().is('.active'))
            return;
        $('.numeros.active').removeClass('active').prev().addClass('active');
        currentPage = currentPage > 1 ? (currentPage - 1) : 1;
        showPage();
    });

    $(".pagination li.pag_next").click(function () {
        if ($(this).prev().is('.active'))
            return;
        $('.numeros.active').removeClass('active').next().addClass('active');
        currentPage = currentPage < totalPages ? (currentPage + 1) : totalPages;
        showPage();
    });
    console.log(currentPage);
});




var trenutna = document.getElementById("trenutna");
var ostale = document.getElementsByClassName("ostale");
for (var i = 0; i < ostale.length; i++) {
    ostale[i].addEventListener("click", prikazi);

}

function prikazi() {
    var sl = this.getAttribute("src");
    trenutna.setAttribute("src", sl);
}


$(document).ready(function () {

    let switchNavMenuItem = (menuItems) => {

        var current = location.pathname

        $.each(menuItems, (index, item) => {

            $(item).removeClass('active')

            if ((current.includes($(item).attr('href')) && $(item).attr('href') !== "/") || ($(item).attr('href') === "/" && current === "/")) {
                $(item).addClass('active')
            }
        })
    }

    $(document).ready(() => {
        switchNavMenuItem($('#nav li a, #nav li link'))
    })


    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

});

var minPrice2 = document.getElementById("min-price");
var maxPrice2 = document.getElementById("max-price");
console.log(minPrice2);
minPrice2.onblur = function () {

    console.log(minPrice2.value);

};

maxPrice2.onblur = function () {
    if (Number(maxPrice2.value) > Number(minPrice2.value)) {
        console.log(maxPrice2.value);
    } else {
        alert("Unesi veći broj od minimalne vrednosti!");
    };
};
