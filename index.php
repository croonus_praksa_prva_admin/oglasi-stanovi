<!DOCTYPE html>
<html lang="en">

<head>
    <!-- HEAD -->
    <?php
    
    include 'layouts/head.php';
    
   ?>
        <!-- /HEAD -->

</head>

<body>
    <!-- TOP_NAV -->
    <?php
    
     include 'layouts/top_nav.php';
    
   ?>

        <!-- /TOP_NAV -->


        <!-- HEADER -->
        <?php
    
     include 'layouts/header.php';
    
   ?>

            <!-- /HEADER -->


                <!-- POSLEDJI OGLASI -->
                <?php
    
     include 'layouts/poslednji_oglasi.php';
    
   ?>

                    <!-- /POSLEDJI OGLASI  -->

                    <!-- MAIN -->

                    <?php
    
    include 'layouts/main.php';
    
   ?>

                        <!-- /MAIN -->


                        <!-- FOOTER -->

                        <?php
    
    include 'layouts/footer.php';
    
   ?>

                            <!-- /FOOTER -->

                            <!-- RESOURCES_SCRIPT -->

                            <?php
    
    include 'layouts/resources_script.php';
    
   ?>

                                <!-- /RESOURCES_SCRIPT -->



</body>

</html>
