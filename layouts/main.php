<section class="thumbs" id="thumbs">
    <div class="container caption-main text-center">
        <h5 class="text-center bottom-border-custom">SVI OGLASI</h5>
    </div>

    <!-- detaljna pretraga-->

    <section class="container detaljna-pretraga">


        <div class="row input-text-polja">


            <div class="col-12 col-md-4 text-left grad">
                <span>Grad</span>
                <select class="custom-select" id="inputGroupSelect01">
                                            <option selected>Izaberi grad</option>
                                            <option value="1">Beograd</option>
                                            <option value="2">Čačak</option>
                                        </select>
            </div>
            <div class="col-12 col-md-4 raspon text-left">

                <span>Kvadratura</span>

                <div class="row kvadratura">

                    <div class="input-group mb-3 col-12 col-sm-12 col-md-6">
                        <input type="text" class="form-control" placeholder="od" aria-label="min" aria-describedby="min">
                        <div class="input-group-append">
                            <span class="input-group-text">m²</span>
                        </div>
                    </div>
                    <!-- /input-group -->

                    <div class="input-group mb-3 col-12 col-sm-12 col-md-6">
                        <input type="text" class="form-control" placeholder="do" aria-label="max" aria-describedby="max">
                        <div class="input-group-append">
                            <span class="input-group-text">m²</span>
                        </div>
                    </div>
                    <!-- /input-group 2-->

                </div>



            </div>

            <div class="col-12 raspon col-md-4 text-left align-self-start">

                <span>Cena</span>

                <div class="row cena">

                    <div class="input-group mb-3 col-12 col-sm-12 col-md-6">
                        <input type="number" id="min-price" class="form-control" placeholder="min" aria-label="min" aria-describedby="min">
                        <div class="input-group-append">
                            <span class="input-group-text">€</span>
                        </div>
                    </div>
                    <!-- /input-group 3 -->

                    <div class="input-group mb-3 col-12 col-sm-12 col-md-6">
                        <input type="number" id="max-price" class="form-control" placeholder="max" aria-label="max" aria-describedby="max">
                        <div class="input-group-append">
                            <span class="input-group-text">€</span>
                        </div>
                    </div>
                    <!-- /input-group 4-->

                </div>

            </div>


            <div class="col-md-4 col-12 col-sm-12  text-left grad">
                <span>Deo grada</span>
                <select class="custom-select" id="inputGroupSelect01">
                                           <option selected>Izaberi deo grada</option>
                                           <option value="1">Barajevo</option>
                                            <option value="2">Čukarica</option>
                                            <option value="3">Grocka</option>
                                            <option value="4">Lorem Ipsum</option>
                                        </select>
            </div>
            <div class="col-md-4 col-12  raspon text-left">

                <span>Naselje</span>
                <select class="custom-select" id="inputGroupSelect01">
                                            <option selected>Izaberi naselje</option>
                                            <option value="1">25 maj</option>
                                            <option value="2">Ada Ciganlija</option>
                                            <option value="3">Avala</option>
                                            <option value="4">Banjica</option>
                                        </select>



            </div>

            <div class="col-md-4 col-12 col-sm-12  raspon text-left align-self-start">

                <span>Broj soba</span>

                <div class="row cena">

                    <div class="input-group mb-3 col-12 col-sm-12 col-md-6">
                        <input type="text" class="form-control" placeholder="od" aria-label="min" aria-describedby="min">

                    </div>
                    <!-- /input-group 3 -->

                    <div class="input-group mb-3 col-12 col-sm-12 col-md-6">
                        <input type="text" class="form-control" placeholder="do" aria-label="max" aria-describedby="max">

                    </div>
                    <!-- /input-group 4-->

                </div>

            </div>










            <div class="col-md-4 col-12 col-sm-12 text-left grad">
                <span>Nameštenost</span>
                <select class="custom-select" id="inputGroupSelect01">
                                            <option selected>Izaberi</option>
                                            <option value="1">Sve</option>
                                            <option value="2">Namešten</option>
                                            <option value="3">Polunamešten</option>
                                            <option value="4">Nenamešten</option>
                                        </select>
            </div>
            <div class="col-md-4 col-12 col-sm-12 raspon text-left">

                <span>Grejanje</span>

                <select class="custom-select" id="inputGroupSelect01">
                                            <option selected>Izaberi</option>
                                            <option value="1">Sve</option>
                                            <option value="2">Centralno</option>
                                            <option value="3">Gas</option>
                                            <option value="4">Električno</option>
                                            <option value="5">Lorem Ipsum</option>
                                        </select>



            </div>

            <div class="col-md-4 col-12 col-sm-12 text-left align-self-start">

                <span>ID Oglasa</span>

                <div class="row cena">

                    <div class="input-group mb-3 col-12 col-sm-12 col-md-6">
                        <input type="text" class="form-control" placeholder="npr:17056" aria-label="na primer: 17056" aria-describedby="na primer: 17056">

                    </div>
                    <!-- /input-group 3 -->

                    <div class="input-group mb-3 col-12 col-sm-12 col-md-6">

                        <button class="btn btn-primary" id="pretrazi-filter-dugme">Pretraži</button>

                    </div>
                    <!-- /input-group 3 -->

                </div>

            </div>

        </div>







    </section>
    <!-- /detaljna pretraga -->




    <div class="container contents-thumbs">
        <div class="row">
            <?php for ($g = 0; $g < 48; $g++) { ?>
            <div class="col-12 col-md-4 col-lg-4 content-thumb">
                <!-- ovo sto se kopira da bude brojac za broj stranica -->
                <a href="detalji.php" class="thumbs-wrapper ">
                    <div class="container-fluid img-box">
                        <img src="img/slika1.jpg" alt="oglas1" id="thumbimg" />

                        <p class="advertisement-ofre">ID00001</p>
                        <div class="block-title1">
                            <h6 class="between">Beograd <span class="text-right">230 €</span></h6>
                            <small class="between">Svetomira Djukica <span>34 m<sup>2</sup></span></small>


                        </div>
                    </div>
                    <p class="subimgTitle between">Izdavanje <span>STAN</span></p>
                </a>


            </div>
            <?php } ?>
        </div>
        <nav class="text-center">
            <ul class="pagination pagination-sm justify-content-center">
                <li class="page-item pag_prev">
                    <a class="page-link" href="#thumbs" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li class="page-item pag_next">
                    <a class="page-link" href="#thumbs" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>




</section>
