<!-- <section style="height:100vh;"> -->

<!-- PROMOCIJA AKCIJA OBAVESTENJA  -->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <!-- Slide One - Set the background image for this slide in the line below -->
        <div class="carousel-item active" style="background-image: url('img/slider.jpg')">
            <div class="carousel-caption naslov-slidera-1">
                <h3>Akcija</h3>
                <p>Nešto veoma povoljno</p>
            </div>
        </div>
        <!-- Slide Two - Set the background image for this slide in the line below -->
        <div class="carousel-item" style="background-image: url('img/poslednji_oglasi9.jpeg')">
            <div class="carousel-caption naslov-slidera-1">
                <h3>Promocija</h3>
                <p>Promo materijal - nešto novo</p>
            </div>
        </div>
        <!-- Slide Three - Set the background image for this slide in the line below -->
        <div class="carousel-item" style="background-image: url('img/poslednji_oglasi10.jpeg')">
            <div class="carousel-caption naslov-slidera-1">
                <h3>Obaveštenja</h3>
                <p>Najnovije aktuelnosti</p>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
</div>




<div class="container main-search">
    <div class="row">
        <div class="col-md-12">
            <h5 class="text-center bottom-border-custom">PRETRAŽITE STANOVE ZA IZDAVANJE</h5>
        </div>
        <div class="col-12 col-sm-12 col-md-2 text-left grad">
            <span>Grad</span>
            <select class="custom-select" id="inputGroupSelect01">
                                                      <option selected>Izaberi grad</option>
                                                      <option value="1">Beograd</option>
                                                      <option value="2">Čačak</option>
                                                  </select>
        </div>
        <div class="col-12 col-sm-12 raspon col-md-4 text-left">

            <span>Kvadratura</span>

            <div class="row kvadratura">

                <div class="input-group mb-3 col-12 col-md-6">
                    <input type="text" class="form-control" placeholder="od" aria-label="min" aria-describedby="min">
                    <div class="input-group-append">
                        <span class="input-group-text">m²</span>
                    </div>
                </div>
                <!-- /input-group -->

                <div class="input-group mb-3 col-12 col-md-6">
                    <input type="text" class="form-control" placeholder="do" aria-label="max" aria-describedby="max">
                    <div class="input-group-append">
                        <span class="input-group-text">m²</span>
                    </div>
                </div>
                <!-- /input-group 2-->

            </div>



        </div>

        <div class="col-12 col-sm-12 raspon col-md-4 text-left align-self-start">

            <span>Cena</span>

            <div class="row cena">

                <div class="input-group mb-3 col-12 col-md-6">
                    <input type="text" class="form-control" placeholder="min" aria-label="min" aria-describedby="min">
                    <div class="input-group-append">
                        <span class="input-group-text">€</span>
                    </div>
                </div>
                <!-- /input-group 3 -->

                <div class="input-group mb-3 col-12 col-md-6">
                    <input type="text" class="form-control" placeholder="max" aria-label="max" aria-describedby="max">
                    <div class="input-group-append">
                        <span class="input-group-text">€</span>
                    </div>
                </div>
                <!-- /input-group 4-->

            </div>

        </div>

        <div class="col-12 align-self-center text-left col-md-2 col-sm-12"><button class="btn button-primary" id="pretrazi-filter-dugme">Pretraži</button></div>

    </div>
    <!-- /row -->
</div>
<!-- /main-search -->



<!-- </section> -->
<!-- 100vh -->
