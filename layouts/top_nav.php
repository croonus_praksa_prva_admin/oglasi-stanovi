<!-- TOP NAV  -->

<section class="top-nav">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="index.php">
                          <img src="img/Logo100.png" alt="logo">
                      </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                      </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto" id="nav">
                <li class="nav-item">
                    <a class="nav-link active text-center" href="index.php">Početna<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item text-center">
                    <a class="nav-link" href="osajtu.php">O sajtu</a>
                </li>

                <li class="nav-item text-center">
                    <a class="nav-link" href="kontakt.php">Kontakt</a>
                </li>
            </ul>
        </div>
    </nav>
</section>


<!-- /TOP NAV  -->
