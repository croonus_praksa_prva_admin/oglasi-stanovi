<section id="footer">

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <h5>Kontakt</h5>
                <ul>
                    <li>
                        <i class="fas fa-map-marker-alt"></i> 9 Jugovića 18,
                        <br>32000 Čačak, Serbia</li>
                    <li>
                        <i class="fa fa-phone"></i> +381 (0) 32 401 342</li>
                    <li>
                        <i class="fa fa-envelope"></i> info@stanovi.com</li>
                    <li>
                        <i class="fa fa-phone"></i> +381 (0) 64 35 10 126</li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <h5>O sajtu</h5>
                <p>Naš sajt sa preko 110.000 oglasa je mesto gde žive kuće i stanovi. Svakog meseca preko 450.000 ljudi poseti preko 12 miliona stranica.</p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <h5>Newsletter</h5>
                <p>Ako želite da dobijate newsletter, molimo registrujte se ovde.</p>
                <form action="">
                    <input type="text" name="email" placeholder="Vaš email">
                    <button type="submit" class="red-submit">Pošalji</button>
                </form>
            </div>
        </div>
    </div>
</section>

<!-- FOOTER -->
<footer id="main-footer" class="text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4>
                    <a href="https://www.facebook.com/" target="_blank" class="fab fa-facebook"></a>
                    <a href="https://www.instagram.com/" target="_blank" class="fab fa-instagram"></a>
                    <a href="https://www.linkedin.com/" target="_blank" class="fab fa-linkedin"></a>
                    <a href="https://www.youtube.com/" target="_blank" class="fab fa-youtube"></a>
                </h4>
            </div>
            <div class="col-md-12">
                <p>Copyright &copy; 2018 | Stanovi Izdavanje</p>
            </div>

        </div>
    </div>
</footer>
