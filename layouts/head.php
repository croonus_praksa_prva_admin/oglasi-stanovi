  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css"/>
  <!-- Fontawesome -->
  <link rel="stylesheet" href="fontawesome-free-5.2.0-web/css/all.min.css"/>
  <!-- custom style.css -->
  <link rel="stylesheet" href="css/style.css"/>
  <title>Početna | Stanovi</title>
