<!-- PROMOCIJA AKCIJA OBAVESTENJA  -->
<section class="container promocija">

    <div class="najnovije row">
        <p class="mx-auto">NAJNOVIJA OBAVEŠTENJA, PONUDE I AKCIJE</p>
    </div>


    <!-- Slider Akcije, Ponude, Obaveštenja -->

    <div id="slider-ponude" class="carousel slide carousel-fade" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#slider-ponude" data-slide-to="0" class="active">
                <img src="img/stil101.jpg" alt="slide1">
            </li>
            <li data-target="#slider-ponude" data-slide-to="1">
                <img src="img/stil102.jpg" alt="slide1">
            </li>
            <li data-target="#slider-ponude" data-slide-to="2">
                <img src="img/stil103.jpg" alt="slide1">
            </li>

            <li data-target="#slider-ponude" data-slide-to="3">
                <img src="img/stil105.jpg" alt="slide1">
            </li>

            <li data-target="#slider-ponude" data-slide-to="4">
                <img src="img/stil106.jpg" alt="slide1">
            </li>
            <li data-target="#slider-ponude" data-slide-to="5">
                <img src="img/stil108.jpg" alt="slide1">
            </li>
        </ol>
        <div class="carousel-inner mx-auto">

            <div class="carousel-item active">
                <img class="d-block w-100" src="img/stil101.jpg" alt="First slide">
                <div class="carousel-caption d-md-block">
                    <h5>Akcija</h5>
                    <p>Nešto veoma povoljno...</p>
                </div>
            </div>

            <div class="carousel-item">
                <img class="d-block w-100" src="img/stil102.jpg" alt="Second slide">
                <div class="carousel-caption d-md-block">
                    <h5>Promocija</h5>
                    <p>Neki promo materijal - nešto novo...</p>
                </div>
            </div>

            <div class="carousel-item">
                <img class="d-block w-100" src="img/stil103.jpg" alt="Third slide">
                <div class="carousel-caption d-md-block">
                    <h5>Nova Ponuda</h5>
                    <p>Novo iz kolekcije ...</p>
                </div>
            </div>

            <div class="carousel-item">
                <img class="d-block w-100" src="img/stil105.jpg" alt="slide1">
                <div class="carousel-caption d-md-block">
                    <h5>Obaveštenje</h5>
                    <p>Neko obaveštenje...</p>
                </div>
            </div>

            <div class="carousel-item">
                <img class="d-block w-100" src="img/stil106.jpg" alt="slide1">
                <div class="carousel-caption d-md-block">
                    <h5>Najnovija ponuda</h5>
                    <p>Nešto zanimljivo...</p>
                </div>
            </div>

            <div class="carousel-item">
                <img class="d-block w-100" src="img/stil108.jpg" alt="slide1">
                <div class="carousel-caption  d-md-block">
                    <h5>Promo usluga</h5>
                    <p>Nešto novo i zanimljivo..</p>
                </div>
            </div>

        </div>

    </div>


</section>
<!-- /#slider-promo -->

<!-- end PROMOCIJA AKCIJA OBAVESTENJA  -->
