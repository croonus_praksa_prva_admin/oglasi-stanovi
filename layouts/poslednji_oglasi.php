<section class="container last-ad">

    <h5 class="text-center bottom-border-custom">POSLEDNJI OGLASI</h5>


    <div id="last-ad" class="carousel slide row" data-ride="carousel">


        <div class="carousel-inner col-md-10 col-12 col-sm-12">

            <div class="carousel-item active">
                <img class="d-block w-100" src="img/poslednji_oglasi7.jpeg" alt="First slide">
                <div class="carousel-caption d-block d-sm-none text-center">
                    <p class="price-small-p">Čačak - ul. Kralja Petra 23 <span class="price-small">230 &euro;</span>
                    </p>
                    <a href="detalji.php"><button class="btn btn-secondary btn-orange">Detaljnije</button></a>

                </div>
                <div class="carousel-caption d-none d-sm-block">
                    <h4 class="title-last-ad">

                        <span class="text-grad-ulica">
                            
                            Čačak - ul. Kralja Petra 23 <span class="price-block">230 &euro;</span>

                        </span>

                        <a href="detalji.php"><button class="btn btn-secondary btn-orange">Detaljnije</button></a>

                    </h4>


                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="img/poslednji_oglasi2.jpeg" alt="Second slide">

                <div class="carousel-caption d-block d-sm-none text-center">
                    <p class="price-small-p">Čačak - ul. Ustanička 13 <span class="price-small">200 &euro;</span> </p>
                    <a href="detalji.php"><button class="btn btn-secondary btn-orange">Detaljnije</button></a>
                </div>
                <div class="carousel-caption d-none d-sm-block">


                    <h4 class="title-last-ad">

                        <span class="text-grad-ulica">
                            
                            Čačak - ul. Ustanička 13 <span class="price-block">200 &euro;</span>

                        </span>

                        <a href="detalji.php"><button class="btn btn-secondary btn-orange">Detaljnije</button></a>

                    </h4>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="img/poslednji_oglasi3.jpeg" alt="Third slide">

                <div class="carousel-caption d-block d-sm-none text-center">
                    <p class="price-small-p">Čačak - ul. Svetog Save 23 <span class="price-small">250 &euro;</span> </p>
                    <a href="detalji.php"><button class="btn btn-secondary btn-orange">Detaljnije</button></a>
                </div>
                <div class="carousel-caption d-none d-sm-block">


                    <h4 class="title-last-ad">

                        <span class="text-grad-ulica">
                            
                            Čačak - ul. Svetog Save 23 <span class="price-block">250 &euro;</span>

                        </span>

                        <a href="detalji.php"><button class="btn btn-secondary btn-orange">Detaljnije</button></a>

                    </h4>

                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="img/poslednji_oglasi4.jpeg" alt="Third slide">

                <div class="carousel-caption d-block d-sm-none text-center">
                    <p class="price-small-p">Beograd - ul. Svetislava Ristića 6 <span class="price-small">400 &euro;</span> </p>
                    <a href="detalji.php"><button class="btn btn-secondary btn-orange">Detaljnije</button></a>
                </div>
                <div class="carousel-caption d-none d-sm-block">

                    <h4 class="title-last-ad">

                        <span class="text-grad-ulica">
                            
                            Beograd - ul. Svetislava Ristića 6 <span class="price-block">400 &euro;</span>

                        </span>

                        <a href="detalji.php"><button class="btn btn-secondary btn-orange">Detaljnije</button></a>

                    </h4>


                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="img/poslednji_oglasi5.jpeg" alt="Third slide">

                <div class="carousel-caption d-block d-sm-none text-center">
                    <p class="price-small-p">Beograd - ul. Žarka Zrenjanina 1 <span class="price-small">630 &euro;</span> </p>
                    <a href="detalji.php"><button class="btn btn-secondary btn-orange">Detaljnije</button></a>
                </div>
                <div class="carousel-caption d-none d-sm-block">


                    <h4 class="title-last-ad">

                        <span class="text-grad-ulica">
                            
                            Beograd - ul. Žarka Zrenjanina 1 <span class="price-block">630 &euro;</span>

                        </span>

                        <a href="detalji.php"><button class="btn btn-secondary btn-orange">Detaljnije</button></a>

                    </h4>

                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="img/poslednji_oglasi6.jpeg" alt="Third slide">

                <div class="carousel-caption d-block d-sm-none text-center">
                    <p class="price-small-p">Beograd - ul. Save Kovačevića 10 <span class="price-small">550 &euro;</span> </p>
                    <a href="detalji.php"><button class="btn btn-secondary btn-orange">Detaljnije</button></a>
                </div>
                <div class="carousel-caption d-none d-sm-block">


                    <h4 class="title-last-ad">

                        <span class="text-grad-ulica">
                            
                            Beograd - ul. Save Kovačevića 10 <span class="price-block">550 &euro;</span>

                        </span>

                        <a href="detalji.php"><button class="btn btn-secondary btn-orange">Detaljnije</button></a>

                    </h4>

                </div>
            </div>

            <a class="carousel-control-prev" href="#last-ad" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>

            <a class="carousel-control-next" href="#last-ad" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>


        </div>

        <ol class="carousel-indicators col-md-2 col-sm-12">

            <li data-target="#last-ad" data-slide-to="0" class="active col-md-12 col-4 col-sm-4">
                <span>Čačak 230 &euro;</span>
            </li>
            <li data-target="#last-ad" data-slide-to="1" class="col-md-12 col-4 col-sm-4">
                <span>Čačak 200 &euro;</span>
            </li>
            <li data-target="#last-ad" data-slide-to="2" class="col-md-12 col-4 col-sm-4">
                <span>Čačak 250 &euro;</span>
            </li>
            <li data-target="#last-ad" data-slide-to="3" class="col-md-12 col-4 col-sm-4">
                <span>Beograd 400 &euro;</span>
            </li>
            <li data-target="#last-ad" data-slide-to="4" class="col-md-12 col-4 col-sm-4">
                <span>Beograd 630 &euro;</span>
            </li>
            <li data-target="#last-ad" data-slide-to="5" class="col-md-12 col-4 col-sm-4">
                <span>Beograd 550 &euro;</span>
            </li>

        </ol>

    </div>

</section>
