<!DOCTYPE html>
<html lang="en">

<head>
    <title>Administrator Login</title>
    <!-- HEAD -->
    <?php
    include 'layouts/head.php';
?>
        <!-- /HEAD -->

</head>

<body>

    <div class="wrapper1">

        <section class="container" id="page-login">

            <div class="row">

                <div class="col-md-4 mx-auto">

                    <img src="img/Logo100.png" alt="logo">

                </div>

                <div class="w-100"></div>


                <div class="col-md-4 mx-auto">

                    <form action="" method="post">

                        <div class="form-group">

                            <label for="exampleInputEmail1">Korisničko ime ili adresa e-pošte</label>

                            <input type="email" class="form-control" id="emaillogin" aria-describedby="emailHelp" placeholder="">


                        </div>

                        <div class="form-group">

                            <label for="exampleInputPassword1">Lozinka</label>

                            <input type="password" class="form-control" id="passlogin" placeholder="">

                        </div>

                        <div class="form-group form-check">

                            <input type="checkbox" class="form-check-input" id="checklog">

                            <label class="form-check-label" for="checklog">Zapamti me</label>

                        </div>

                        <button type="submit" class="btn btn-primary"><a href="admin-forma.php">Prijava</a></button>

                    </form>

                </div>

                <div class="w-100"></div>

                <div class="col-md-4 mx-auto back-to-main">

                    <p><a href="index.php">← Nazad na stanovi.rs</a></p>

                </div>

            </div>
            <!-- /.row  -->

        </section>

    </div>
    <!-- /#wrapper -->

    <!-- RESOURCES_SCRIPT -->
    <?php
    include 'layouts/resources_script.php';
?>
        <!-- /RESOURCES_SCRIPT -->
</body>

</html>
