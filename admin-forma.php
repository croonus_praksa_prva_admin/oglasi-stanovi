<!DOCTYPE html>
<html lang="en">

<head>
    <title>Administrator Form Input</title>
    <!-- HEAD -->
    <?php
    include 'layouts/head.php';
?>
        <!-- /HEAD -->

</head>

<body>

    <div class="wrapper">

        <section class="container" id="admin-form">

            <div class="row title-admin-form">
                <h2>Unesite podatke vezane za nekretninu</h2>
            </div>

            <div class="">

                <form action="" method="post" class="row">

                    <div class="form-group col-md-12">

                        <label for="naslovOglasa">Naslov Oglasa</label>

                        <input type="text" class="form-control" id="naslovOglasa" aria-describedby="naslovOglasa" placeholder="npr. Moderan stan, nova zgrada ...">

                    </div>
                    <!-- /naslov oglasa -->

                    <div class="form-group col-md-12">

                        <label for="podnaslovOglasa">Podnaslov Oglasa</label>

                        <input type="text" class="form-control" id="podnaslovOglasa" aria-describedby="podnaslovOglasa" placeholder="npr. Beograd - Grocka - Banjica">

                    </div>
                    <!-- /podnaslov oglasa -->

                    <div class="form-group col-md-4">

                        <label for="grad">Grad</label>

                        <input type="text" class="form-control" id="grad" aria-describedby="grad" placeholder="npr. Beograd">

                    </div>
                    <!-- /naziv grada -->

                    <div class="form-group col-md-4">

                        <label for="kvadratura">Kvadratura</label>

                        <input type="text" class="form-control" id="kvadratura" aria-describedby="kvadratura" placeholder="npr. 40">

                    </div>
                    <!-- /kvadratura stana -->

                    <div class="form-group col-md-4">

                        <label for="cena">Cena</label>

                        <input type="text" class="form-control" id="cena" aria-describedby="cena" placeholder="npr. 200">

                    </div>
                    <!-- /cena stana -->

                    <div class="form-group col-md-4">

                        <label for="deoGrada">Deo Grada</label>

                        <input type="text" class="form-control" id="deoGrada" aria-describedby="deoGrada" placeholder="npr. Grocka">

                    </div>
                    <!-- /deo grada -->

                    <div class="form-group col-md-4">

                        <label for="naselje">Naselje</label>

                        <input type="text" class="form-control" id="naselje" aria-describedby="naselje" placeholder="npr. Banjica">

                    </div>
                    <!-- /naselje -->

                    <div class="form-group col-md-4">

                        <label for="ulica">Ulica</label>

                        <input type="text" class="form-control" id="ulica" aria-describedby="ulica" placeholder="npr. Svetislava Ristića 6">

                    </div>
                    <!-- /ulica -->

                    <div class="form-group col-md-3">

                        <label for="namestenost">Nameštenost</label>

                        <input type="text" class="form-control" id="namestenost" aria-describedby="namestenost" placeholder="namešten/nenamešten">

                    </div>
                    <!-- /nameštenost -->

                    <div class="form-group col-md-3">

                        <label for="grejanje">Grejanje</label>

                        <input type="text" class="form-control" id="grejanje" aria-describedby="grejanje" placeholder="npr. CG">

                    </div>
                    <!-- /grejanje -->

                    <div class="form-group col-md-3">

                        <label for="brojSoba">Broj Soba</label>

                        <input type="text" class="form-control" id="brojSoba" aria-describedby="brojSoba" placeholder="npr. 3">

                    </div>
                    <!-- /broj soba -->

                    <div class="form-group col-md-3">

                        <label for="idOglasa">ID Oglasa</label>

                        <input type="text" class="form-control" id="idOglasa" aria-describedby="idOglasa" placeholder="npr. 00005">

                    </div>
                    <!-- /id oglasa -->

                    <div class="form-group col-md-6">

                        <label for="dodatno1">Dodatno 1 </label>

                        <input type="text" class="form-control" id="dodatno1" aria-describedby="dodatno1" placeholder="npr. Useljiv">

                    </div>
                    <!-- /broj soba -->

                    <div class="form-group col-md-6">

                        <label for="dodatno2">Dodatno 2</label>

                        <input type="text" class="form-control" id="dodatno2" aria-describedby="dodatno2" placeholder="npr. Depozit">

                    </div>

                    <div class="form-group col-md-4">

                        <label for="ostalo1">Ostalo 1</label>

                        <input type="text" class="form-control" id="ostalo1" aria-describedby="ostalo1" placeholder="npr. terasa">

                    </div>
                    <!-- /ostalo 1 -->

                    <div class="form-group col-md-4">

                        <label for="ostalo2">Ostalo 2</label>

                        <input type="text" class="form-control" id="ostalo2" aria-describedby="ostalo2" placeholder="">

                    </div>
                    <!-- /ostalo 2 -->

                    <div class="form-group col-md-4">

                        <label for="ostalo3">Ostalo 3</label>

                        <input type="text" class="form-control" id="ostalo3" aria-describedby="ostalo3" placeholder="">

                    </div>
                    <!-- /ostalo 3 -->

                    <div class="form-group col-md-4">

                        <label for="ostalo4">Ostalo 4</label>

                        <input type="text" class="form-control" id="ostalo4" aria-describedby="ostalo4" placeholder="">

                    </div>
                    <!-- /ostalo 4 -->

                    <div class="form-group col-md-4">

                        <label for="ostalo5">Ostalo 5</label>

                        <input type="text" class="form-control" id="ostalo5" aria-describedby="ostalo5" placeholder="">

                    </div>
                    <!-- /ostalo 5 -->

                    <div class="form-group col-md-4">

                        <label for="ostalo6">Ostalo 6</label>

                        <input type="text" class="form-control" id="ostalo6" aria-describedby="ostalo6" placeholder="">

                    </div>
                    <!-- /ostalo 6 -->

                    <div class="form-group col-md-4">

                        <label for="ostalo7">Ostalo 7</label>

                        <input type="text" class="form-control" id="ostalo7" aria-describedby="ostalo7" placeholder="">

                    </div>
                    <!-- /ostalo 7 -->

                    <div class="form-group col-md-4">

                        <label for="ostalo8">Ostalo 8</label>

                        <input type="text" class="form-control" id="ostalo8" aria-describedby="ostalo8" placeholder="">

                    </div>
                    <!-- /ostalo 8 -->

                    <div class="form-group col-md-4">

                        <label for="ostalo9">Ostalo 9</label>

                        <input type="text" class="form-control" id="ostalo9" aria-describedby="ostalo9" placeholder="">

                    </div>
                    <!-- /ostalo 9 -->

                    <div class="form-group col-md-12">

                        <label for="dodatniOpis">Dodatni Opis</label>

                        <textarea class="form-control" id="dodatniOpis" rows="4"></textarea>

                    </div>
                    <!-- /dodatni opis -->


                    <div class="form-group col-md-6" id="ubacivanjeSlika">

                        <label for="slikeStana">Ubacite slike stana</label>

                        <input type="file" class="form-control-file" id="slikeStana">

                    </div>

                    <div class="w-100"></div>

                    <div class="form-group col-md-6">

                        <button type="submit" class="btn btn-primary"><a href="">Postavi Oglas</a></button>

                    </div>


                </form>

            </div>
            <!-- /.row  -->

        </section>
        <!-- / #admin-form -->

    </div>
    <!-- /#wrapper -->

    <!-- RESOURCES_SCRIPT -->
    <?php
    include 'layouts/resources_script.php';
?>
        <!-- /RESOURCES_SCRIPT -->
</body>

</html>
