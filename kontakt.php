<!DOCTYPE html>
<html lang="en">

<head>
   <!-- HEAD --> 
   <title>Kontakt | Stanovi</title>
   <?php
    
    include 'layouts/head.php';
    
   ?> 
   <!-- /HEAD --> 
    
</head>
<body>
   <!-- TOP_NAV -->
   <?php
    
     include 'layouts/top_nav.php';
    
   ?>
   
   <!-- /TOP_NAV -->
   
    <section class="container kontakt-info100">

            <div class="row omotac-info100">

                <div class="col-12 col-md-6 info100">

                    <p><span>Info</span></p>

                    <ul class="info-tel">
                        <li><span><i class="fas fa-phone-square"></i></span>Tel: +381 (0)32 555 669</li>
                        <li><span><i class="fas fa-envelope-square"></i></span>E-mail: stanovi@info.com</li>  
                        <li><span><i class="fas fa-map-marker-alt"></i></span>Adresa: Kralja Petra 23</li>
                    </ul>

                </div><!-- /col-6 col-md-6 info100 -->

                <div class="col-12 col-md-6 info100">

                    <p>Radno vreme</p>

                    <ul class="info-tel">
                        <li><span><i class="fas fa-check-square"></i></span>Ponedeljak-Petak: 07:00h-15:00h</li>
                        <li><span><i class="fas fa-check-square"></i></span>Subota-Nedelja: zatvoreno</li>  

                    </ul>

                </div><!-- /col-6 col-md-6 info100 -->

            </div><!-- /row omotac-info100 -->

            <div class="row kontakt-forme">

                <div class="col-12 col-md-6 kontakt-generalno">

                    <form>
                        <p>Postavite pitanje putem forme</p>
                        <div class="form-group">
                            <label for="ime">Ime</label>
                            <input type="text" class="form-control" id="ime" aria-describedby="imehelp" placeholder="Vaše ime">
                            
                        </div>
                        <div class="form-group">
                            <label for="prezime">Prezime</label>
                            <input type="text" class="form-control" id="prezime" placeholder="Vaše prezime">
                        </div>
                        <div class="form-group">
                            <label for="telefon">Telefon</label>
                            <input type="text" class="form-control" id="telefon" placeholder="Vaš br. telefona">
                        </div>
                        <div class="form-group">
                            <label for="email">E-mail <span class="zvezda">*</span></label>
                            <input type="email" class="form-control" id="email" placeholder="Vaš email" required>
                        </div>
                          <div class="form-group">
                            <label for="text-area">Kako možemo da Vam pomognemo? <span class="zvezda">*</span></label>
                            <textarea class="form-control" id="text-area" rows="3" placeholder="Vaša poruka" required></textarea>
                          </div>
                        
                        
                        <button type="submit" class="btn btn-primary posalji-dugme">Pošalji</button>
                    </form>

                </div><!-- /.col-6 .col-md-6 .kontakt-generalno -->
                
                <!-- LOKACIJA -->
                
                <div class="col-12 col-md-6 mapa">
                    
                     <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2875.3999933612736!2d20.350898815386966!3d43.88898174517018!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475772152bd4337f%3A0x7ccbff561dcfb7b2!2z0JTQtdCy0LXRgiDQiNGD0LPQvtCy0LjRm9CwLCDQp9Cw0YfQsNC6!5e0!3m2!1ssr!2srs!4v1533550059151" width="600" height="515" frameborder="0" allowfullscreen></iframe>
                    
                </div><!-- /mapa -->

            </div><!-- /.row .kontakt-forme -->

        </section><!-- /container-fluid kontakt-info100 -->
   
   
   
   
   
   
   
   <!-- FOOTER -->
    
   <?php
    
    include 'layouts/footer.php';
    
   ?>
   
   <!-- /FOOTER -->
   
   <!-- RESOURCES_SCRIPT -->
  
   <?php
    
    include 'layouts/resources_script.php';
    
   ?>
   
   <!-- /RESOURCES_SCRIPT -->
   
</body>

    

</html>



