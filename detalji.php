<!DOCTYPE html>
<html lang="en">

<head>
    <!-- HEAD -->
    <title>Detalji | Stanovi</title>
    <?php
        include 'layouts/head.php';
        ?>
        <!-- /HEAD -->

</head>

<body>
    <!-- TOP_NAV -->
    <?php
include 'layouts/top_nav.php';
?>

        <!-- /TOP_NAV -->

        <!-- Page Detalji -->

        <div id="detalji">
            <!-- Prikaz detalja -->
            <div class="container">
                <div class="row col-md-12">

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php">Početna</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Detalji</li>
                        </ol>
                    </nav>

                </div>
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
                        <!--<h3 class="naslov">Detalji:</h3> -->
                        <h5 class="naslov">Moderan stan, nova zgrada, garaža!</h5>
                        <span>Beograd - Grocka - Banjica</span>
                        <hr>
                    </div>
                    <div class="col-sm-6 col-xs-12 col-md-6 col-lg-6">
                        <img class="img-fluid" src="img/stan.png" id="trenutna">

                        <div id="ostale">
                            <img src="img/stan.png" alt="" class="ostale">
                            <img src="img/stan2.jpg" alt="" class="ostale">
                            <img src="img/stan3.jpg" alt="" class="ostale">
                            <img src="img/stan1.jpg" alt="" class="ostale">

                        </div>
                    </div>

                    <div class="col-sm-6 col-xs-12 col-md-6 col-lg-6">
                        <table class="table text-center">
                            <tr>
                                <th class="top-border">Grad</th>
                                <th class="top-border">Kvadratura</th>
                                <th class="top-border">Cena</th>
                            </tr>

                            <tr>
                                <td>Beograd</td>
                                <td>35 m²</td>
                                <td>250 €</td>
                            </tr>

                        </table>
                        <table class="table">
                            <tr>
                                <td class="top-border">Deo grada</td>
                                <th class="top-border">Grocka</th>
                            </tr>
                            <tr>
                                <td>Naselje</td>
                                <th>Banjica</th>
                            </tr>
                            <tr>
                                <td>Ulica</td>
                                <th>Svetislava Ristića 6</th>
                            </tr>

                            <tr>
                                <td>Nameštenost</td>
                                <th>Namešten</th>
                            </tr>
                            <tr>
                                <td>Grejanje</td>
                                <th>CG</th>
                            </tr>

                            <tr>
                                <td>Broj soba</td>
                                <th>3</th>
                            </tr>
                            <tr>
                                <td>ID Oglasa</td>
                                <th>12056</th>
                            </tr>

                        </table>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                        <table class="table">
                            <tr>
                                <th class="top-border">Dodatno</th>
                            </tr>
                            <tr>
                                <td>Useljiv</td>
                                <td>Depozit</td>
                                <td></td>
                            </tr>
                            <tr>
                                <th class="top-border">Ostalo</th>
                            </tr>
                            <tr>
                                <td>Terasa</td>
                                <td>Klima</td>
                                <td>Lift</td>
                            </tr>
                            <tr>
                                <td>Telefon</td>
                                <td>KTV</td>
                                <td>Internet</td>
                            </tr>
                            <tr>
                                <td>Interfon</td>
                                <td>Video nadzor</td>
                                <td>Garaža</td>
                            </tr>
                        </table>
                        <table class="table">
                            <tr>
                                <th class="top-border">Dodatni Opis</th>
                            </tr>
                            <tr>
                                <td>Moderan stan sa 3 sobe u novoj zgradi. Sastoji se od dnevne sobe, potpuno opremljene kuhinje, kupatila, dve spavaće sobe i terase. Stan ima garažno mesto.</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <!-- FOOTER -->

        <?php
include 'layouts/footer.php';
?>

            <!-- /FOOTER -->

            <!-- RESOURCES_SCRIPT -->

            <?php
        include 'layouts/resources_script.php';
        ?>

                <!-- /RESOURCES_SCRIPT -->

</body>

</html>
