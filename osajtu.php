<!DOCTYPE html>
<html lang="en">

<head>
    <!-- HEAD -->
    <title>O Sajtu | Stanovi</title>
    <?php
        include 'layouts/head.php';
        ?>
        <!-- /HEAD -->

</head>

<body>
    <!-- TOP_NAV -->
    <?php
include 'layouts/top_nav.php';
?>

        <!-- /TOP_NAV -->

        <section class="o_sajtu_pozadina">

        </section>
        <section id="o_sajtu">

            <div class="container o_sajtu">
                <div class="row">


                    <div class="text-center col-md-12">
                        <h1 class="bottom-border-custom">O sajtu</h1>
                    </div>
                </div>
                <!-- .row naslov -->

                <div class="row">

                    <div class="col-md-6 col-sm-12"><img src="img/stil101.jpg" alt="" class="img-fluid img_osajtu"></div>

                    <div class="col-md-6 text-osajtu col-sm-12">
                        Naš sajt sa preko 110.000 oglasa je mesto gde žive kuće i stanovi. Svakog meseca preko 450.000 ljudi poseti preko 12 miliona stranica. To nas cini oglasnikom broj 1 za nekretnine u Srbiji. Tim cini 8 posvecenih osoba koji žele da vam kupovinu, prodaju ili izdavanje stana, kuce ili bilo koje druge nekretnine ucini najlakšom mogucom. Upravo zbog toga smo i razvili opciju Mi tražimo za vas koja na vaš e-mail šalje sve najnovije nekretnine po kriterijumima koje ste sami izabrali. Na taj nacin prvi možete doci do doma vaših snova.
                    </div>

                </div>
                <!-- .row prvi -->

                <div class="row">



                    <div class="col-md-6 text-osajtu col-sm-12">
                        Saradujemo sa preko 380 agencija iz cele Srbije i 60 investitora i stalno rastemo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec consequat laoreet sem nec facilisis. Morbi eu vulputate leo. Integer sed pellentesque tortor, ultricies congue ex. In eu quam sed ligula condimentum venenatis. Phasellus vulputate dapibus finibus. Fusce massa lectus, cursus a erat sed, placerat tristique lacus. Vivamus eget bibendum lectus, eget elementum sapien. Donec non ullamcorper odio, vel condimentum risus. Vivamus placerat odio lorem, nec aliquam lacus vehicula at. Nunc nec s uscipit metus, in ultricies dolor. Ut rhoncus commodo ultrices.
                    </div>

                    <div class="col-md-6 col-sm-12"><img src="img/stil102.jpg" alt="" class="img-fluid img_osajtu"></div>

                </div>
                <!-- .row drugi -->

                <div class="row">
                    <div class="col-md-12 text-osajtu col-sm-12">

                        In euismod dapibus elit in eleifend. Mauris scelerisque ut turpis sit amet tempus. Donec hendrerit accumsan lorem, ut tempus ante maximus at. Cras vulputate consequat enim, non sagittis nulla consequat a. Cras laoreet nunc non metus rutrum, vel dictum massa malesuada. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi non imperdiet risus. Donec elit dui, pulvinar a enim et, viverra sagittis enim. Donec id mattis lorem, vel vestibulum tortor. Mauris consequat gravida gravida. Proin vulputate, erat ac bibendum effcitur, arcu lectus convallis sem, quis suscipit velit erat vitae elit. Aliquam vehicula facilisis massa a auctor.

                    </div>
                </div>
            </div>
        </section>



        <!-- FOOTER -->

        <?php
include 'layouts/footer.php';
?>

            <!-- /FOOTER -->

            <!-- RESOURCES_SCRIPT -->

            <?php
        include 'layouts/resources_script.php';
        ?>

                <!-- /RESOURCES_SCRIPT -->
</body>

</html>
